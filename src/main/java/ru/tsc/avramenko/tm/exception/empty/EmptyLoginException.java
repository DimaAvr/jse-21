package ru.tsc.avramenko.tm.exception.empty;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty.");
    }

}
