package ru.tsc.avramenko.tm.api.repository;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    List<Task> findAll(String userId, Comparator<Task> comparator);

    boolean existsById(String userId, String id);

    List<Task> findAllTaskByProjectId(String userId, final String id);

    Task bindTaskToProjectById(String userId, final String projectId, final String taskId);

    Task unbindTaskById(String userId, final String id);

    void unbindAllTaskByProjectId(String userId, final String id);

    Task findByName(String userId, String name);

    Task findByIndex(String userId, int index);

    Task removeByName(String userId, String name);

    Task removeByIndex(String userId, int index);

    Task startById(String userId, String id);

    Task startByName(String userId, String name);

    Task startByIndex(String userId, int index);

    Task finishById(String userId, String id);

    Task finishByName(String userId, String name);

    Task finishByIndex(String userId, int index);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByName(String userId, String name, Status status);

    Task changeStatusByIndex(String userId, int index, Status status);

}
