package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.model.AbstractOwnerEntity;

import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    E add(String userId, final E entity);

    void remove(String userId, final E entity);

    List<E> findAll(String userId);

    void clear(String userId);

    E findById(String userId, final String id);

    E removeById(String userId, final String id);

}
