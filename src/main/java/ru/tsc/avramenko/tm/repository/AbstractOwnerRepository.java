package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.repository.IOwnerRepository;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.AbstractOwnerEntity;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public E add(final String userId, final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> listEnt = findAll(userId);
        list.remove(listEnt);
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public void clear(final String userId) {
        final List<String> listEnt = list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        listEnt.forEach(list::remove);
    }

    @Override
    public E findById(final String userId, final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .filter(e -> e.getUserId().equals(userId))
                .findFirst()
                .orElseThrow(ProcessException::new);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final Optional<E> entity =  Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(ProcessException::new);

    }

}