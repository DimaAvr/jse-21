package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class UserShowByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-show-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show user by login.";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth) throw new AccessDeniedException();
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (!isAdmin) {
            if (!currentUserId.equals(user.getId())) throw new AccessDeniedException();
        }
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

}