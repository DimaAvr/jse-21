package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Arrays;

public class UserChangeRoleCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-change-role";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change the user role.";
    }

    @Override
    public void execute() {
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAdmin) throw new AccessDeniedException();
        System.out.println("ENTER USER ID: ");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) throw new UserNotFoundException();
        System.out.println("ENTER NEW ROLE: ");
        System.out.println(Arrays.toString(Role.values()));
        final String roleValue = TerminalUtil.nextLine();
        final Role role = Role.valueOf(roleValue);
        serviceLocator.getUserService().setRole(id, role);
    }

}