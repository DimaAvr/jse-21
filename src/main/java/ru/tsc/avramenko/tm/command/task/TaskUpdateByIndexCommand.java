package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateByIndex(userId, index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

}