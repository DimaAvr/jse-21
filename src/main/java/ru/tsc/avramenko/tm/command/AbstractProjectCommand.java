package ru.tsc.avramenko.tm.command;

import ru.tsc.avramenko.tm.exception.empty.EmptyDescriptionException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Created: " + project.getCreated());
        System.out.println("Start Date: " + project.getStartDate());
        System.out.println("Finish Date: " + project.getFinishDate());
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        return new Project(name, description);
    }

}