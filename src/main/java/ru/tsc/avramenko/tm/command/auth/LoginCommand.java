package ru.tsc.avramenko.tm.command.auth;

import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.command.system.ExitCommand;
import ru.tsc.avramenko.tm.command.user.UserCreateCommand;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login user to system.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        final User user = serviceLocator.getUserService().findByLogin(login);
        System.out.println("Welcome back, " + user.getFirstName() + " " + user.getMiddleName() + " !");
    }

}