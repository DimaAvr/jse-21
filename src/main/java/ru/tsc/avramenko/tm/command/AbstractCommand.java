package ru.tsc.avramenko.tm.command;

import ru.tsc.avramenko.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    @Override
    public String toString() {
        String result = "";
        String name = name();
        String arg = arg();
        String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }
}